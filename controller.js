weatherApp.controller('mainController', ["$scope", "weatherService", "$location", function($scope, weatherService, $location) {
    $scope.city = weatherService.city;
    
    $scope.$watch('city', function(){
        
        weatherService.city = $scope.city;
        
    });
    
    $scope.submit = function() {
        $location.path('/forecast');
    };
}]);

weatherApp.controller('forecastController', ["$scope", "weatherService", "$resource", "$routeParams", function($scope, weatherService, $resource, $routeParams) {
    $scope.city = weatherService.city;
    
    $scope.days = $routeParams.days || 3;
    
    var url = 'http://api.openweathermap.org/data/2.5/forecast/daily?&appid=03d15fd7bfcb3e3040609bd38e4960e6'
    
    $scope.weatherAPI = $resource(url, {
        callback: "JSON_CALLBACK"}, {get: {method: "JSONP"}});
//    $scope.weatherAPI = $resource(url, {city: "@city", cnt: "@cnt"},
//                                 {get: {method:"GET", params: {city: $scope.city, cnt:2}}});
    
    $scope.weatherResult = $scope.weatherAPI.get({q: $scope.city, cnt: $scope.days});
    
    $scope.convertToFahrenheit = function(degK) {
        return (Math.round(((1.8 * (degK - 273)) + 32) * 10) / 10).toFixed(1);
    }
    
    $scope.convertToDate = function(dt) {
        return new Date(dt * 1000);
    }
}]);