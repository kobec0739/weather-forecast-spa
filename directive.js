weatherApp.directive('weatherReport', function() {
    return {
        restrict: 'AE',
        templateUrl: 'directive/weatherReport.html',
        replace: true,
        scope: {
            weatherDay: "=",
            convertToStandard: "&",
            convertToDate: "&",
            dateFormat: "@"
        }
    }
});